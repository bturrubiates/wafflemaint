from setuptools import setup, find_packages

setup(
    name="wafflemaint",
    version="0.1",
    packages=find_packages(),
    install_requires=["jinja2", "dnspython"],
    package_data={"wafflemaint": ["templates/*"]},
)
