maint: maint.c
	gcc maint.c -o maint

clean:
	rm maint

install: maint
	sudo python3 setup.py install
	sudo cp maint /usr/local/bin/maint
	sudo chown root /usr/local/bin/maint
	sudo chmod u+s /usr/local/bin/maint

uninstall:
	sudo rm /usr/local/bin/maint
