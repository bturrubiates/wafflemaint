#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>

int main (int argc, char* argv[]) {
    // setup args "python3 -m wafflemaint argv[...]"
    char** nargv = calloc(argc + 3, sizeof(char*));
    nargv[0] = "/usr/bin/python3";
    nargv[1] = "-m";
    nargv[2] = "wafflemaint";

    int i;
    for (i = 1; i < argc; i++) {
        nargv[2 + i] = argv[i];
    }

    // exec in clean environment
    int result = execve(nargv[0], nargv, NULL);

    // cleanup and return
    free(nargv);
    return result;
}
