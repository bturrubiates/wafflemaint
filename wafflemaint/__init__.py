import argparse
import configparser

PARSER = argparse.ArgumentParser()
PARSER.description = """
This tool allows waffle.tech users to manage certain settings that would
otherwise require setup by an administrator. There are various subcommands to
manage different features, you can run a subcommand with -h or --help for more
information."""
PARSER.prog = "maint"

TOOLS = PARSER.add_subparsers(help="Tools available")

CONFIG = configparser.ConfigParser()

__all__ = ["web", "email"]
