import os
import pwd

import jinja2

# import all tools
from . import *
from . import PARSER

def run():
    loader = jinja2.PackageLoader("wafflemaint")
    env = jinja2.Environment(loader=loader)

    # Get the user's original username
    ruid, euid, _ = os.getresuid()
    if ruid == 0:
        print("This tool is not meant to be run by root.")
        exit(1)
    username = pwd.getpwuid(ruid).pw_name

    # Check for sudo privs before trying anything
    if euid != 0:
        print("This tool must be run via the 'maint' wrapper.")
        exit(1)

    # Parse args and run a tool
    args = PARSER.parse_args()
    if "func" in args:
        args.func(args, env, username)
    else:
        print(PARSER.print_help())


run()
