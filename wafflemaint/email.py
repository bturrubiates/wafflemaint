from . import TOOLS
from .util import reload_unit, check_domain_sanity
import csv, sys, subprocess

MASTER_TABLE = "/etc/postfix/master_virt_table"
SLMAP_FILE = "/etc/postfix/smtpd_sender_login_maps.pcre"
DOMAINS_FILE = "/etc/postfix/virtual_domains"
ALIASES_FILE = "/etc/postfix/virtual_aliases"

def master_read():
    configured_domains = {}
    with open(MASTER_TABLE, 'r') as f:
        for entry in f:
            domain, user = entry.strip().split("\t")
            configured_domains[domain] = user
    return configured_domains

def regen_maps(env):
    configured_domains = master_read()
    # Bunch of steps here - first, the smtpd_sender_login_maps.pcre
    with open(SLMAP_FILE, "w") as f:
        template = env.get_template("slmaps.pcre.tpl")
        f.write(template.render({"domains": configured_domains}))
    # Now the virtual domains table
    with open(DOMAINS_FILE, "w") as f:
        template = env.get_template("virtual_domains.tpl")
        f.write(template.render({"domains": configured_domains}))
    # Now the virtual aliases table
    with open(ALIASES_FILE, "w") as f:
        template = env.get_template("virtual_aliases.tpl")
        f.write(template.render({"domains": configured_domains}))
    # Now we regenerate the indexes for the hash tables
    subprocess.check_call(["/usr/sbin/postmap", DOMAINS_FILE])
    subprocess.check_call(["/usr/sbin/postmap", ALIASES_FILE])
    # And finally load the new configuration
    reload_unit("postfix")

def master_add(env, domain, user):
    with open(MASTER_TABLE, 'a') as f:
        f.write("{}\t{}\n".format(domain, user))
    regen_maps(env)

def master_del(env, del_domain):
    # this is less reentrant but whatever hopefully people don't do it a lot
    configured_domains = master_read()
    del configured_domains[del_domain]
    with open(MASTER_TABLE, 'w') as f:
        for domain, user in configured_domains.items():
            f.write("{}\t{}\n".format(domain, user))
    regen_maps(env)

def sub_email(args, env, user):
    check_domain_sanity(args.domain)
    configured_domains = master_read()
    if not args.remove:
        if args.domain in configured_domains.keys():
            print("This domain is already configured for receiving mail.")
            sys.exit(5)
        master_add(env, args.domain, user)
        print("""
The waffle.tech mail server is now configured to receive mail for {domain}.
For this to actually work, you will need to add the following DNS entries:

{domain} MX 10 mail.waffle.tech
{domain} TXT v=spf1 a mx include:waffle.tech
_dmarc.{domain} TXT v=DMARC1; p=quarantine; rua=mailto:admin-badmail@waffle.tech;
    ruf=mailto:admin-badmail-forensic@waffle.tech; fo=1; adkim=r; aspf=r;
    pct=100; rf=afrf
mail._domainkey.{domain} TXT v=DKIM1; h=sha256; k=rsa;
    p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCl3BETYEdMr/wIhNn16QHQ1CgfAG4WE1NAwY
    WwhtuhKPNEyj1hk5UwA/vrswb5IkBvSDpgkgQmqaBkhBs7Jkm/munHHwPPMMG7vOJJqCs2GLwKmi
    Z2gH/OYEKDHmgzZBm6IiTlZnU6wXgWo1ME9tWLVSeePnMxIi5kXH+fjSrqwQIDAQAB

Yes, these DNS records are all important. If you have any questions about them,
please contact admin@waffle.tech.
""".format(domain=args.domain))
    else: # --remove was specified
        if configured_domains[args.domain] != user:
            print("You don't own this domain.")
            sys.exit(5)
        master_del(env, args.domain)
        print("Done!")

def init():
    "Register this tool with the argument parser"
    parser_email_desc = """
    This tool will configure the waffle.tech mail server to send and receive
    email for an external domain name. There is considerable DNS setup required
    to make this work completely, for which this tool will give you
    instructions."""

    parser_email = TOOLS.add_parser("email", description=parser_email_desc, help="Manage external domain email")
    parser_email.add_argument("--remove", "-r", help="Remove domain", action="store_true")
    parser_email.add_argument("domain", help="Domain name")
    parser_email.set_defaults(func=sub_email)


init()
