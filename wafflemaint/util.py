import subprocess, re

def reload_unit(name):
    "Reload a systemd unit"
    subprocess.call(["/bin/systemctl", "reload", name])

def check_domain_sanity(name):
    "Just checks a domain against a trivial regex and bails if it looks wrong"
    if not re.match(r"^[\w.]+\.[\w]+$", name):
        print("The domain you provided doesn't look right.")
        sys.exit(1)
